package me.declanmc96.Announcer;

import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class Announcer extends JavaPlugin {
  protected List<String> announcementMessages;
  protected String announcementPrefix;
  protected long announcementInterval;
  protected boolean enabled;
  protected boolean random;
  private AnnouncerThread announcerThread;

  public Announcer() {
    this.announcerThread = new AnnouncerThread(this);
  }

  public void onEnable() {
    saveDefaultConfig();
    reloadConfiguration();
    AnnouncerCommandExecutor announcerCommandExecutor = new AnnouncerCommandExecutor(this);
    getCommand("announcer").setExecutor(announcerCommandExecutor);
  }

  public void announce() {
    this.announcerThread.run();
  }

  public void announce(int index) {
    announce(this.announcementMessages.get(index - 1));
  }

  public void announce(String line) {
    String[] messages = line.split("&n");
    for (String message : messages) {
      if (message.startsWith("/")) {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), message.substring(1));
      } else if (Bukkit.getServer().getOnlinePlayers().size() > 0) {
        String messageToSend = ChatColor.translateAlternateColorCodes('&', String.format("%s%s", this.announcementPrefix, message));
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
          if (player.hasPermission("announcer.receiver")) {
            player.sendMessage(messageToSend);
          }
        }
      }
    }
  }

  public void saveConfiguration() {
    getConfig().set("announcement.messages", this.announcementMessages);
    getConfig().set("announcement.interval", this.announcementInterval);
    getConfig().set("announcement.prefix", this.announcementPrefix);
    getConfig().set("announcement.enabled", this.enabled);
    getConfig().set("announcement.random", this.random);
    saveConfig();
  }

  public void reloadConfiguration() {
    reloadConfig();
    this.announcementPrefix = getConfig().getString("announcement.prefix", "&c[Announcement] ");
    this.announcementMessages = getConfig().getStringList("announcement.messages");
    this.announcementInterval = getConfig().getInt("announcement.interval", 1000);
    this.enabled = getConfig().getBoolean("announcement.enabled", true);
    this.random = getConfig().getBoolean("announcement.random", false);
    // Recreate the repeating task in case interval changed
    BukkitScheduler scheduler = getServer().getScheduler();
    scheduler.cancelTasks(this);
    scheduler.scheduleSyncRepeatingTask(this, this.announcerThread, announcementInterval * 20L, announcementInterval * 20L);
  }

  public String getAnnouncementPrefix() {
    return this.announcementPrefix;
  }

  public void setAnnouncementPrefix(String announcementPrefix) {
    this.announcementPrefix = announcementPrefix;
    saveConfig();
  }

  public long getAnnouncementInterval() {
    return this.announcementInterval;
  }

  public void setAnnouncementInterval(long announcementInterval) {
    this.announcementInterval = announcementInterval;
    saveConfiguration();
    BukkitScheduler scheduler = getServer().getScheduler();
    scheduler.cancelTasks(this);
    scheduler.scheduleSyncRepeatingTask(this, this.announcerThread, announcementInterval * 20L, announcementInterval * 20L);
  }

  public void addAnnouncement(String message) {
    this.announcementMessages.add(message);
    saveConfiguration();
  }

  public String getAnnouncement(int index) {
    return this.announcementMessages.get(index - 1);
  }

  public int numberOfAnnouncements() {
    return this.announcementMessages.size();
  }

  public void removeAnnouncements() {
    this.announcementMessages.clear();
    saveConfiguration();
  }

  public void removeAnnouncement(int index) {
    this.announcementMessages.remove(index - 1);
    saveConfiguration();
  }

  public boolean isAnnouncerEnabled() {
    return this.enabled;
  }

  public void setAnnouncerEnabled(boolean enabled) {
    this.enabled = enabled;
    saveConfiguration();
  }

  public boolean isRandom() {
    return this.random;
  }

  public void setRandom(boolean random) {
    this.random = random;
    saveConfiguration();
  }
}
