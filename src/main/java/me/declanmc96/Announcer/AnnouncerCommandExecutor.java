package me.declanmc96.Announcer;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AnnouncerCommandExecutor implements CommandExecutor {
  private final Announcer plugin;

  AnnouncerCommandExecutor(Announcer plugin) {
    this.plugin = plugin;
  }

  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (args.length == 0) {
      onVersionCommand(sender, command, label, args);
      return true;
    }
    switch (args[0].toLowerCase()) {
      case "version":
      case "info":
        onVersionCommand(sender, command, label, args);
        break;
      case "help":
        onHelpCommand(sender, command, label, args);
        break;
      case "add":
        onAddCommand(sender, command, label, args);
        break;
      case "broadcast":
      case "now":
        onBroadcastCommand(sender, command, label, args);
        break;
      case "say":
      case "once":
        onBroadcastOnceCommand(sender, command, label, args);
        break;
      case "list":
        onListCommand(sender, command, label, args);
        break;
      case "delete":
        onDeleteCommand(sender, command, label, args);
        break;
      case "interval":
        onIntervalCommand(sender, command, label, args);
        break;
      case "predix":
        onPrefixCommand(sender, command, label, args);
        break;
      case "random":
        onRandomCommand(sender, command, label, args);
        break;
      case "enable":
        onEnableCommand(sender, command, label, args);
        break;
      case "reload":
        onReloadCommand(sender, command, label, args);
        break;
      default:
        sender.sendMessage(ChatColor.RED + "Invalid arguments! " +
                "Use '/announce help' to get a list of valid commands.");
        break;
    }
    return true;
  }

  void onVersionCommand(CommandSender sender, Command command, String label, String[] args) {
    var authors = String.join(", ", plugin.getDescription().getAuthors());
    sender.sendMessage(
        String.format("%s === %s [Version %s] === ", ChatColor.LIGHT_PURPLE, this.plugin.getDescription().getName(),
            this.plugin.getDescription().getVersion()));
    sender.sendMessage(String.format("Authors: %s", authors));
    sender.sendMessage(String.format("Version: %s", plugin.getDescription().getVersion()));
    sender.sendMessage(String.format("Website: %s", plugin.getDescription().getWebsite()));
    sender.sendMessage("");
    sender.sendMessage(ChatColor.GRAY + "Use '/announce help' to get a list of valid commands.");
  }

  void onHelpCommand(CommandSender sender, Command command, String label, String[] args) {
    sender.sendMessage(String.format("%s === %s [Version %s] === ", ChatColor.LIGHT_PURPLE,
        this.plugin.getDescription().getName(), this.plugin.getDescription().getVersion()));
    if (sender.hasPermission("announcer.add")) {
      sender.sendMessage(ChatColor.GRAY + "/announce add <message>" + ChatColor.WHITE +
          " - Adds a new announcement");
    }
    if (sender.hasPermission("announcer.broadcast")) {
      sender.sendMessage(ChatColor.GRAY + "/announce broadcast [<index>]" + ChatColor.WHITE +
          " - Broadcast an announcement NOW (From the predefined list)");
    }
    if (sender.hasPermission("announcer.broadcast")) {
      sender.sendMessage(ChatColor.GRAY + "/announce say [<message>]" + ChatColor.WHITE +
          " - Broadcast an announcement NOW");
    }
    if (sender.hasPermission("announcer.delete")) {
      sender.sendMessage(ChatColor.GRAY + "/announce delete <index>" + ChatColor.WHITE +
          " - Removes the announcement with the passed index");
    }
    if (sender.hasPermission("announcer.moderate")) {
      sender.sendMessage(ChatColor.GRAY + "/announce enable [true|false]" + ChatColor.WHITE +
          " - Enables or disables the announcer.");
      sender.sendMessage(ChatColor.GRAY + "/announce interval <seconds>" + ChatColor.WHITE +
          " - Sets the seconds between the announcements.");
      sender.sendMessage(ChatColor.GRAY + "/announce prefix <message>" + ChatColor.WHITE +
          " - Sets the prefix for all announcements.");
      sender.sendMessage(ChatColor.GRAY + "/announce list" + ChatColor.WHITE + " - Lists all announcements");
      sender.sendMessage(ChatColor.GRAY + "/announce random [true|false]" + ChatColor.WHITE +
          " - Enables or disables the random announcing mode.");
    }
    if (sender.hasPermission("announcer.admin")) {
      sender.sendMessage(ChatColor.GRAY + "/announce reload" + ChatColor.WHITE + " - Reloads the config.yml");
    }
  }

  void onAddCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.add")) {
      if (args.length > 1) {
        StringBuilder messageToAnnounce = new StringBuilder();
        for (int index = 1; index < args.length; index++) {
          messageToAnnounce.append(args[index]);
          messageToAnnounce.append(" ");
        }
        this.plugin.addAnnouncement(messageToAnnounce.toString());
        sender.sendMessage(ChatColor.GREEN + "Added announcement successfully!");
      } else {
        sender.sendMessage(ChatColor.RED + "You need to pass a message to announce!");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onBroadcastCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.broadcast")) {
      if (args.length == 2) {
        try {
          int index = Integer.parseInt(args[1]);
          if ((index > 0) && (index <= this.plugin.numberOfAnnouncements())) {
            this.plugin.announce(index);
          } else {
            sender.sendMessage(ChatColor.RED + "There isn't any announcement with the passed index!");
            sender.sendMessage(ChatColor.RED + "Use '/announce list' to view all available announcements.");
          }
        } catch (NumberFormatException e) {
          sender.sendMessage(ChatColor.RED + "Index must be a integer!");
        }
      } else if (args.length == 1) {
        this.plugin.announce();
      } else {
        sender.sendMessage(ChatColor.RED + "Invalid number of arguments! Use /announce help to view the help!");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onBroadcastOnceCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.broadcast")) {
      if (args.length >= 2) {
        StringBuilder toSend = new StringBuilder();
        for (int i = 1; args.length != i; i++) {
          toSend.append(args[i]).append(" ");
        }
        this.plugin.announce(toSend.toString());
      } else {
        sender.sendMessage(ChatColor.RED + "No text to broadcast! Use /announce help to view the help!");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onListCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.moderate")) {
      if ((args.length == 1) || (args.length == 2)) {
        int page = 1;
        if (args.length == 2) {
          try {
            page = Integer.parseInt(args[1]);
          } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Invalid page number!");
          }
        }
        sender.sendMessage(ChatColor.GREEN + String.format(" === Announcements [Page %d/%d] ===", page,
            this.plugin.announcementMessages.size() / 7 + 1));
        int indexStart = Math.abs(page - 1) * 7;
        int indexStop = Math.min(page * 7, this.plugin.announcementMessages.size());
        for (int index = indexStart + 1; index <= indexStop; index++) {
          sender.sendMessage(String.format("%d - %s", index, ChatColor.translateAlternateColorCodes('&', this.plugin.getAnnouncement(index))));
        }
      } else {
        sender.sendMessage(ChatColor.RED + "Invalid number of arguments! Use '/announce help' to view the help.");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onDeleteCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.delete")) {
      if (args.length == 2) {
        try {
          int index = Integer.parseInt(args[1]);
          if ((index > 0) && (index <= this.plugin.numberOfAnnouncements())) {
            sender.sendMessage(String.format("%sRemoved announcement: '%s'", ChatColor.GREEN, this.plugin.getAnnouncement(index)));
            this.plugin.removeAnnouncement(index);
          } else {
            sender.sendMessage(ChatColor.RED + "There isn't any announcement with the passed index!");
            sender.sendMessage(ChatColor.RED + "Use '/announce list' to view all available announcements.");
          }
        } catch (NumberFormatException e) {
          sender.sendMessage(ChatColor.RED + "Index must be a integer!");
        }
      } else {
        sender.sendMessage(ChatColor.RED + "Too many arguments! Use '/announce help' to view the help.");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onIntervalCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.moderate")) {
      if (args.length == 2) {
        try {
          this.plugin.setAnnouncementInterval(Integer.parseInt(args[1]));
          sender.sendMessage(ChatColor.GREEN + "Set interval of scheduled announcements successfully!");
        } catch (NumberFormatException e) {
          sender.sendMessage(ChatColor.RED + "Interval must be a number!");
        } catch (ArithmeticException e) {
          sender.sendMessage(ChatColor.RED + "Interval must be greater than 0!");
        }
      } else if (args.length == 1) {
        sender.sendMessage(String.format("%sPeriod duration is %d", ChatColor.LIGHT_PURPLE, this.plugin.getAnnouncementInterval()));
      } else {
        sender.sendMessage(ChatColor.RED + "Too many arguments! Use '/announce help' to view the help!");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onPrefixCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.moderate")) {
      if (args.length > 1) {
        StringBuilder prefixBuilder = new StringBuilder();
        for (int index = 1; index < args.length; index++) {
          prefixBuilder.append(args[index]);
          prefixBuilder.append(" ");
        }
        this.plugin.setAnnouncementPrefix(prefixBuilder.toString());
        sender.sendMessage(ChatColor.GREEN + "Set prefix for all announcements successfully!");
      } else {
        sender.sendMessage(String.format("%sPrefix is %s", ChatColor.LIGHT_PURPLE,
            ChatColor.translateAlternateColorCodes('&', this.plugin.getAnnouncementPrefix())));
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onRandomCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.moderate")) {
      if (args.length == 2) {
        if ("true".equalsIgnoreCase(args[1])) {
          this.plugin.setRandom(true);
          sender.sendMessage(ChatColor.GREEN + "Random mode enabled!");
        } else if ("false".equalsIgnoreCase(args[1])) {
          this.plugin.setRandom(false);
          sender.sendMessage(ChatColor.GREEN + "Sequential mode enabled!");
        } else {
          sender.sendMessage(ChatColor.RED + "Use true or false to enable or disable! Use '/announce help' to view the help.");
        }
      } else if (args.length == 1) {
        if (this.plugin.isRandom()) {
          sender.sendMessage(ChatColor.LIGHT_PURPLE + "Random mode is enabled.");
        } else {
          sender.sendMessage(ChatColor.LIGHT_PURPLE + "Sequential mode is enabled.");
        }
      } else {
        sender.sendMessage(ChatColor.RED + "Invalid number of arguments! Use '/announce help' to view the help.");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onEnableCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.moderate")) {
      if (args.length == 2) {
        if ("true".equalsIgnoreCase(args[1])) {
          this.plugin.setAnnouncerEnabled(true);
          sender.sendMessage(ChatColor.GREEN + "Announcer enabled!");
        } else if ("false".equalsIgnoreCase(args[1])) {
          this.plugin.setAnnouncerEnabled(false);
          sender.sendMessage(ChatColor.GREEN + "Announcer disabled!");
        } else {
          sender.sendMessage(ChatColor.RED + "Use ture or false to enable or disable! Use '/announce help' to view the help.");
        }
      } else if (args.length == 1) {
        if (this.plugin.isRandom()) {
          sender.sendMessage(ChatColor.LIGHT_PURPLE + "Announcer is enabled.");
        } else {
          sender.sendMessage(ChatColor.LIGHT_PURPLE + "Announcer is disabled.");
        }
      } else {
        sender.sendMessage(ChatColor.RED + "Invalid number of arguments! Use '/announce help' to view the help.");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }

  void onReloadCommand(CommandSender sender, Command command, String label, String[] args) {
    if (sender.hasPermission("announcer.moderate")) {
      if (args.length == 1) {
        this.plugin.reloadConfiguration();
        sender.sendMessage(ChatColor.LIGHT_PURPLE + "Configuration reloaded.");
      } else {
        sender.sendMessage(ChatColor.RED + "No arguments needed! Use '/announce help' to view the help.");
      }
    } else sender.sendMessage(ChatColor.RED + "You don't have permission.");
  }
}
